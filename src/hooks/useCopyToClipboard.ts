import { useCallback } from 'react';

type UseCopyToClipboardReturnType = (text: string) => boolean;

export const useCopyToClipboard = (): UseCopyToClipboardReturnType => {
  const copyToClipboard = useCallback((text: string) => {
    if (!navigator.clipboard) {
      // Clipboard API not available
      return false;
    }
    navigator.clipboard.writeText(text);
    return true;
  }, []);

  return copyToClipboard;
};
