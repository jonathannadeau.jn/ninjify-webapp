import { useCallback, useState } from 'react';

interface UseLocalStorageReturnType<ValueType> {
  value: ValueType;
  setValue: (value: ValueType) => void;
}

export const useLocalStorage = <ValueType>(
  key: string
): UseLocalStorageReturnType<ValueType> => {
  const [value, setValue] = useState<ValueType>(() => {
    const value = localStorage.getItem(key);
    if (value) {
      return JSON.parse(value);
    }
    return null;
  });

  const setLocalstorageValue = useCallback(
    (value: ValueType) => {
      setValue(value);
      localStorage.setItem(key, JSON.stringify(value));
    },
    [key]
  );

  return {
    value,
    setValue: setLocalstorageValue,
  };
};
