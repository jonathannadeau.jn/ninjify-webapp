import { Alert as BAlert } from 'react-bootstrap';
import style from './alert.module.scss';

type Variant =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark';

export interface AlertProps {
  label: string;
  variant: Variant;
  show: boolean;
  onClose?: () => void;
}

export const Alert = ({ label, variant, show, onClose }: AlertProps) => {
  return (
    <BAlert
      onClose={onClose}
      show={show}
      variant={variant}
      dismissible
      transition={true}
      className={style.alert}>
      {label}
    </BAlert>
  );
};
