/* eslint-disable @typescript-eslint/no-unused-vars */
import style from './namesHistory.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopy } from '@fortawesome/free-regular-svg-icons';
import { Alert } from '../alert';
import { useEffect, useState, MouseEventHandler } from 'react';
import { AlertProps } from '../alert';
import { useCopyToClipboard } from '../../hooks';
import { faBars, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Offcanvas } from 'react-bootstrap';

interface NamesHistoryProps {
  ninjaNames: string[];
  setValues: (values: string[]) => void;
}

export const NamesHistory = ({ ninjaNames, setValues }: NamesHistoryProps) => {
  const [menuShow, setMenuShow] = useState<boolean>(false);
  const copyToClipboard = useCopyToClipboard();
  const [alertProps, setAlertProps] = useState<AlertProps>({
    label: '',
    variant: 'success',
    show: false,
  });

  const handleMenuShow = (show: boolean) => setMenuShow(show);

  const historyOnDelete = () => {
    try {
      setValues([]);
      setAlertProps({
        label: 'History deleted!',
        variant: 'success',
        show: true,
      });
      handleMenuShow(false);
    } catch (error) {
      setAlertProps({
        label: 'Failed to delete history!',
        variant: 'danger',
        show: true,
      });
    }
  };

  const handleCopyToClipboard: MouseEventHandler<HTMLButtonElement> = (
    event
  ) => {
    const ninjaName = event.currentTarget.textContent;
    if (!ninjaName) {
      return;
    }

    const copied = copyToClipboard(ninjaName);

    if (!copied) {
      return setAlertProps({
        label: 'Failed to copy to clipboard!',
        variant: 'danger',
        show: true,
      });
    }
    handleMenuShow(false);
    setAlertProps({
      label: 'Copied to clipboard!',
      variant: 'success',
      show: true,
    });
  };

  useEffect(() => {
    if (alertProps.show) {
      const timeout = setTimeout(() => {
        setAlertProps({ ...alertProps, show: false });
      }, 2000);

      return () => clearTimeout(timeout);
    }
  }, [alertProps.show]);

  return (
    <div className={style.namesHistory}>
      <Offcanvas show={menuShow} onHide={() => handleMenuShow(false)}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title className={style.namesHistory__OffcanvasTitle}>
            <h2 className={`${style.namesHistory__title} fs-4`}>
              Names History{' '}
              <button
                className={style.namesHistory__trashButton}
                onClick={() => historyOnDelete()}>
                <FontAwesomeIcon
                  icon={faTrash}
                  className={style.namesHistory__trashIcon}
                />
              </button>
            </h2>
          </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <div className={style.namesHistory__menuNames}>
            {ninjaNames.map((ninjaName) => (
              <button
                key={ninjaName}
                className={style.namesHistory__button}
                onClick={handleCopyToClipboard}>
                {ninjaName} <FontAwesomeIcon icon={faCopy} />
              </button>
            ))}
          </div>
        </Offcanvas.Body>
      </Offcanvas>
      <Alert
        {...alertProps}
        onClose={() => setAlertProps({ ...alertProps, show: false })}
      />
      <h2 className={`${style.namesHistory__title} fs-4`}>
        Names History{' '}
        <button
          className={style.namesHistory__trashButton}
          onClick={() => historyOnDelete()}>
          <FontAwesomeIcon
            icon={faTrash}
            className={style.namesHistory__trashIcon}
          />
        </button>
        <button className={style.namesHistory__menuButton}>
          <FontAwesomeIcon
            icon={faBars}
            className={style.namesHistory__menuIcon}
            onClick={() => handleMenuShow(true)}
          />
        </button>
      </h2>
      <div className={style.namesHistory__names}>
        {ninjaNames.map((ninjaName) => (
          <button
            key={ninjaName}
            className={style.namesHistory__button}
            onClick={handleCopyToClipboard}>
            {ninjaName} <FontAwesomeIcon icon={faCopy} />
          </button>
        ))}
      </div>
    </div>
  );
};
