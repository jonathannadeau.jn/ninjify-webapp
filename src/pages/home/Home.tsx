/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import { Button } from 'react-bootstrap';
import { API_URL } from '../../constants';
import style from './home.module.scss';
import { useCopyToClipboard, useLocalStorage } from '../../hooks';
import { NamesHistory, Alert, AlertProps } from '../../components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserNinja } from '@fortawesome/free-solid-svg-icons';
import { faCopy } from '@fortawesome/free-regular-svg-icons';

const { Select, Label } = Form;

const options = [
  { value: 'programmingLanguages', label: 'Programming Languages' },
  { value: 'frontEnd', label: 'Front End development' },
  { value: 'backEnd', label: 'Back End development' },
  { value: 'databases', label: 'Databases' },
  { value: 'cloud', label: 'Cloud' },
];

export const Home = () => {
  const [alertProps, setAlertProps] = useState<AlertProps>({
    label: '',
    variant: 'success',
    show: false,
  });
  const [selectedOption, setSelectedOption] = useState<string>(
    options[0].value
  );
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [ninjaName, setNinjaName] = useState<string>('');
  const { setValue, value: ninjaNames } =
    useLocalStorage<string[]>('ninjaNames');
  const copyToClipboard = useCopyToClipboard();

  const handleCopyToClipboard = () => {
    const copied = copyToClipboard(ninjaName);
    if (!copied) {
      return setAlertProps({
        label: 'Failed to copy to clipboard!',
        variant: 'danger',
        show: true,
      });
    }

    setAlertProps({
      label: 'Copied to clipboard!',
      variant: 'success',
      show: true,
    });
  };

  const setAlertShow = (show: boolean) => {
    setAlertProps({ ...alertProps, show });
  };

  const getSuperNinjaName = async () => {
    try {
      setIsLoading(true);
      const topic = selectedOption;
      const url = `${API_URL}ninjify?x=${topic}`;
      const response = await fetch(url, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      if (!ninjaNames) {
        setValue([data.ninjaName]);
      } else {
        setValue([...ninjaNames, data.ninjaName].reverse());
      }
      setNinjaName(data.ninjaName);
    } catch (error: unknown) {
      if (error instanceof Error) {
        setAlertProps({
          label: error.message,
          variant: 'danger',
          show: true,
        });
      }
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (alertProps.show) {
      const timeout = setTimeout(() => {
        setAlertShow(false);
      }, 2000);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [alertProps.show]);

  return (
    <>
      <Alert {...alertProps} onClose={() => setAlertShow(false)} />
      <main className={style.home}>
        <h1 className={style.home__title}>
          Ninjify{' '}
          <FontAwesomeIcon icon={faUserNinja} className={style.home__icon} />
        </h1>
        <div className={style.home__body}>
          {ninjaNames && (
            <aside className={style.home__namesHistoryContainer}>
              <NamesHistory ninjaNames={ninjaNames} setValues={setValue} />
            </aside>
          )}
          <div className={style.home__form}>
            <div className={style.home__inputContainer}>
              <Label className={style.home__label} htmlFor="topic">
                Choose a topic
              </Label>
              <Select
                value={selectedOption}
                onChange={(event) => setSelectedOption(event.target.value)}>
                {options.map((option) => (
                  <option value={option.value} key={option.value}>
                    {option.label}
                  </option>
                ))}
              </Select>
            </div>
            <Button
              className={style.home__button}
              disabled={isLoading}
              variant="primary"
              type="button"
              onClick={() => {
                if (!isLoading) {
                  getSuperNinjaName();
                }
              }}>
              {isLoading ? 'Loading...' : 'Get your Super Ninja Name'}
            </Button>
            {ninjaName && (
              <div
                className={`${style.home__result} skranji`}
                onClick={() => handleCopyToClipboard()}>
                {ninjaName}
                <span className={`${style.home__result_iconContainer} skranji`}>
                  copy{' '}
                  <FontAwesomeIcon
                    icon={faCopy}
                    className={style.home__result_icon}
                  />
                </span>
              </div>
            )}
          </div>
        </div>
      </main>
    </>
  );
};
